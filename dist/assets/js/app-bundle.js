(function() {
    "use strict";
    angular.module("app.core.services", [ "ngResource" ]);
    angular.module("app.core.filters", []);
    angular.module("app.core.attributes", []);
    angular.module("app.core.elements", []);
    angular.module("app.core", [ "app.core.services", "app.core.filters", "app.core.attributes", "app.core.elements" ]);
})();

(function() {
    "use strict";
    const filters = angular.module("app.core.filters");
    filters.filter("encodeId", function() {
        return function encodeId(inValue) {
            if (inValue === undefined || inValue.length === 0) return inValue; else return inValue.replace(/[$]/g, "dollar_");
        };
    });
})();

(function() {
    "use strict";
    const filters = angular.module("app.core.filters");
    filters.filter("objectFilter", function() {
        return function objectFilter(items, filter) {
            debugger;
            if (!filter) return items;
            const result = {};
            angular.forEach(filter, function(filterVal, filterKey) {
                angular.forEach(items, function(item, key) {
                    const fieldVal = item[filterKey];
                    if (fieldVal && fieldVal.toLowerCase().indexOf(filterVal.toLowerCase()) > -1) result[key] = item;
                });
            });
            return result;
        };
    });
})();

(function() {
    "use strict";
    const filters = angular.module("app.core.filters");
    filters.filter("orderByKeys", function() {
        return function orderByKeys(objectToSort) {
            const sortedByKeysMap = {};
            Object.keys(objectToSort).sort().forEach(key => sortedByKeysMap[key] = objectToSort[key]);
            return sortedByKeysMap;
        };
    });
})();

(function() {
    "use strict";
    const module = angular.module("app.core.elements");
    module.component("appHeader", {
        bindings: {
            project: "="
        },
        controller: class AppHeaderComponent {},
        template: `\n<a class="navbar-brand" style="color: white;" ng-href="{{$ctrl.project.url}}">\n  <svg class="navbar-nav-svg" style="vertical-align: initial;" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 499.36" focusable="false"><title>GitHub</title><path d="M256 0C114.64 0 0 114.61 0 256c0 113.09 73.34 209 175.08 242.9 12.8 2.35 17.47-5.56 17.47-12.34 0-6.08-.22-22.18-.35-43.54-71.2 15.49-86.2-34.34-86.2-34.34-11.64-29.57-28.42-37.45-28.42-37.45-23.27-15.84 1.73-15.55 1.73-15.55 25.69 1.81 39.21 26.38 39.21 26.38 22.84 39.12 59.92 27.82 74.5 21.27 2.33-16.54 8.94-27.82 16.25-34.22-56.84-6.43-116.6-28.43-116.6-126.49 0-27.95 10-50.8 26.35-68.69-2.63-6.48-11.42-32.5 2.51-67.75 0 0 21.49-6.88 70.4 26.24a242.65 242.65 0 0 1 128.18 0c48.87-33.13 70.33-26.24 70.33-26.24 14 35.25 5.18 61.27 2.55 67.75 16.41 17.9 26.31 40.75 26.31 68.69 0 98.35-59.85 120-116.88 126.32 9.19 7.9 17.38 23.53 17.38 47.41 0 34.22-.31 61.83-.31 70.23 0 6.85 4.61 14.81 17.6 12.31C438.72 464.97 512 369.08 512 256.02 512 114.62 397.37 0 256 0z" fill="currentColor" fill-rule="evenodd"></path></svg>\n  {{$ctrl.project.name}}\n  <span class="navbar-brand text-muted" ng-bind="'v' + $ctrl.project.version"></span>\n</a>\n\x3c!--\n<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">\n  <span class="navbar-toggler-icon"></span>\n</button>\n--!>\n`
    });
})();

(function() {
    "use strict";
    const module = angular.module("app.core.elements");
    module.component("sidebar", {
        transclude: true,
        bindings: {
            onFilterChanged: "&"
        },
        controller: class TreeSidebarComponent {
            $onInit() {
                this.filterText = "";
            }
        },
        template: `\n<form class="bd-search d-flex align-items-center">\n  <input class="form-control" type="text" ng-model="$ctrl.filterText" ng-change="$ctrl.onFilterChanged({text: $ctrl.filterText})" placeholder="Filter items">\n</form>\n<nav class="collapse bd-links" ng-transclude></div>\n`
    });
})();

(function() {
    "use strict";
    const module = angular.module("app.core.elements");
    module.component("tree", {
        transclude: true,
        controller: class TreeComponent {},
        template: '<div class="tree" ng-transclude></div>'
    });
    module.component("treeList", {
        transclude: true,
        controller: class TreeListComponent {},
        template: '<ul class="tl" role="menu" ng-transclude></ul>'
    });
    module.component("treeNode", {
        transclude: true,
        controller: class TreeNodeComponent {},
        template: '<li class="tn" role="listitem" ng-transclude></li>'
    });
    module.component("treeItem", {
        transclude: true,
        controller: class TreeItemComponent {},
        template: '<a class="ti ti-{{skin}}" role="link" ng-transclude></a>',
        scope: {
            skin: "@"
        }
    });
})();

(function() {
    "use strict";
    const module = angular.module("app.core.attributes");
    module.directive("scrollTo", function() {
        return {
            restrict: "A",
            link: function(scope, element, attrs) {
                element.bind("click", function(event) {
                    let scrollToSelector = null;
                    let scrollToOffset = 0;
                    let preventDefault = false;
                    if (attrs.href !== undefined && attrs.href !== "") {
                        preventDefault = true;
                        scrollToSelector = attrs.href;
                    } else {
                        scrollToSelector = attrs.scrollTo;
                    }
                    attrs.scrollToOffset = attrs.scrollToOffset || $("body").attr("scroll-to-offset");
                    if (attrs.scrollToOffset !== undefined && attrs.scrollToOffset !== "") {
                        scrollToOffset = attrs.scrollToOffset;
                    }
                    if (scrollToSelector === undefined || scrollToSelector === "") {
                        return;
                    }
                    const $scrollToSelector = $(scrollToSelector);
                    if ($scrollToSelector.length === 0) {
                        throw new Error("scrollTo directive: " + scrollToSelector + " does not exist.");
                    }
                    const beforeScrollTo = $("[before-scroll-to]", $scrollToSelector);
                    const afterScrollTo = $("[after-scroll-to]", $scrollToSelector);
                    const scrollToTop = $scrollToSelector.offset().top;
                    const currentTop = $("header").offset().top;
                    const duration = currentTop !== scrollToTop - scrollToOffset ? 1e3 : 0;
                    $("html, body").stop().animate({
                        scrollTop: scrollToTop - scrollToOffset
                    }, {
                        duration: duration,
                        easing: "easeInOutExpo",
                        start: function() {
                            let index;
                            let $element;
                            for (index = 0; index < afterScrollTo.length; index++) {
                                $element = $(afterScrollTo[index]);
                                $element.removeClass($element.attr("after-scroll-to"));
                            }
                            for (index = 0; index < beforeScrollTo.length; index++) {
                                $element = $(beforeScrollTo[index]);
                                $element.addClass($element.attr("before-scroll-to"));
                            }
                        },
                        complete: function() {
                            let index;
                            let $element;
                            for (index = 0; index < beforeScrollTo.length; index++) {
                                $element = $(beforeScrollTo[index]);
                                $element.removeClass($element.attr("before-scroll-to"));
                            }
                            for (index = 0; index < afterScrollTo.length; index++) {
                                $element = $(afterScrollTo[index]);
                                $element.addClass($element.attr("after-scroll-to"));
                            }
                        }
                    });
                    if (preventDefault === true) {
                        event.preventDefault();
                    }
                });
            }
        };
    });
})();

(function() {
    "use strict";
    angular.module("app.yuidoc.services", [ "ngResource" ]);
    angular.module("app.yuidoc.elements", [ "ngSanitize", "app.yuidoc.services" ]);
    angular.module("app.yuidoc", [ "app.yuidoc.services", "app.yuidoc.elements" ]);
})();

(function() {
    "use strict";
    const services = angular.module("app.yuidoc.services");
    services.service("yuidocDataService", function($resource, querifySync, yuidocDataPath) {
        function YuidocDataService() {
            this.dataset_ = null;
            this.loaded = false;
        }
        YuidocDataService.prototype = {
            load: function() {
                const paramDefaults = {};
                const actions = {
                    cache: true
                };
                const this_ = this;
                return $resource(yuidocDataPath, paramDefaults, actions).get().$promise.then(function(dataset) {
                    this_.loaded = true;
                    this_.dataset_ = dataset;
                });
            },
            get dataset() {
                if (this.dataset_ === null) {
                    throw Error("YuidocDataService: dataset is not loaded.");
                }
                return this.dataset_;
            },
            getProject: function() {
                return this.dataset.project;
            },
            getModules: function() {
                return this.dataset.modules;
            },
            getClasses: function() {
                return this.dataset.classes;
            },
            getClassItems: function() {
                return this.dataset.classitems;
            },
            getGlobalClasses: function() {
                const query = {
                    $not: {
                        $has: "module"
                    }
                };
                return querifySync.extract(this.dataset.classes, query);
            },
            getClassesByModuleName: function(moduleName) {
                const query = {
                    module: moduleName
                };
                return querifySync.extract(this.dataset.classes, query);
            },
            getClassItemsByModuleAndClassName: function(moduleName, className) {
                const query = {
                    module: moduleName,
                    class: className
                };
                return querifySync.filter(this.dataset.classitems, query);
            }
        };
        return new YuidocDataService();
    });
})();

(function() {
    "use strict";
    const module = angular.module("app.yuidoc.elements");
    module.component("classItemAttribute", {
        binding: {
            classItem: "<"
        },
        controller: class classItemAttributeComponent {},
        template: `\n    <div>\n      <div class="bs-callout markdown-body">\n        <a href="#" ng-bind="$ctrl.classItem.type"></a>\n        <span ng-bind="$ctrl.classItem.name"></span>\n        <span ng-if="$ctrl.classItem.access==='private'" class="badge badge--dark">private</span>\n      </div>\n    </div>\n`
    });
})();

(function() {
    "use strict";
    const module = angular.module("app.yuidoc.elements");
    module.component("classItemMethod", {
        bindings: {
            classItem: "<"
        },
        controller: [ "$sce", "marked", class ClassItemMethodComponent {
            constructor($sce, marked) {
                this.$sce = $sce;
                this.marked = marked;
            }
            $onInit() {
                const {classItem: classItem, marked: marked, $sce: $sce} = this;
                if (classItem.return && classItem.return.description) {
                    classItem.return.description = $sce.trustAsHtml(marked(classItem.return.description));
                }
            }
        } ],
        template: `\n<div>\n  <div class="syntax">\n    <strong class="text-muted">Syntax</strong>\n\n    <div class="bs-callout markdown-body">\n      <code>\n        <span ng-bind="$ctrl.classItem.name"></span>(<span ng-repeat="$param in $ctrl.classItem.params">\n            <span ng-bind="$param.name"></span><span ng-if="$index < $ctrl.classItem.params.length-1">, </span>\n        </span>);\n      </code>\n    </div>\n  </div>\n\n  <class-item-params class-item="$ctrl.classItem"></class-item-params>\n\n  <div class="returns" ng-if="$ctrl.classItem.return !== undefined">\n    <strong class="text-muted">Returns</strong>\n    <br/><br/>\n    <ul>\n      <li>\n        <a href="#" ng-bind="$ctrl.classItem.return.type"></a>\n\n        <div class="param-description">\n          <p class="description markdown-body" ng-bind-html="$ctrl.classItem.return.description"></p>\n        </div>\n      </li>\n    </ul>\n  </div>\n\n</div>\n`
    });
})();

(function() {
    "use strict";
    const module = angular.module("app.yuidoc.elements");
    module.component("classItemParams", {
        bindings: {
            classItem: "<"
        },
        controller: [ "$sce", "marked", class ClassItemParamsComponent {
            constructor($sce, marked) {
                this.marked = marked;
                this.$sce = $sce;
            }
            $onInit() {
                const {$sce: $sce, classItem: classItem, marked: marked} = this;
                if (!classItem.params) return;
                for (let index = 0; index < classItem.params.length; index++) {
                    const param = classItem.params[index];
                    param.description = $sce.trustAsHtml(marked(param.description));
                }
            }
        } ],
        template: `\n<div class="params" ng-if="$ctrl.classItem.params !== undefined">\n  <div ng-if="$ctrl.classItem.itemtype==='method'">\n    <strong class="text-muted">Parameters</strong>\n    <br/>\n  </div>\n  <br/>\n  <ul>\n    <li ng-repeat="$param in $ctrl.classItem.params">\n      <div>\n        <a href="#" ng-bind="$param.type"></a> <code class="param-name" ng-bind="$param.name"></code>\n      </div>\n      <div ng-if="$param.description !== undefined" class="param-description">\n        <p class="description markdown-body" ng-bind-html="$param.description"></p>\n      </div>\n      <class-item-props ng-if="$param.props !== undefined" class-item="$param"></class-item-props>\n    </li>\n  </ul>\n</div>\n`
    });
})();

(function() {
    "use strict";
    const module = angular.module("app.yuidoc.elements");
    module.component("classItemProperty", {
        bindings: {
            classItem: "<"
        },
        controller: class ClassItemPropertyComponent {},
        template: `\n<div>\n  <ul>\n    <li>\n      <a href="#" ng-bind="$ctrl.classItem.type"></a> <span ng-bind="$ctrl.classItem.name"></span>\n      <class-item-params ng-if="$ctrl.classItem.params !== undefined" class-item="$ctrl.classItem"></class-item-params>\n    </li>\n  </ul>\n</div>\n`
    });
})();

(function() {
    "use strict";
    const module = angular.module("app.yuidoc.elements");
    module.component("classItemProps", {
        bindings: {
            classItem: "<"
        },
        controller: [ "$sce", "marked", class ClassItemPropsComponent {
            constructor($sce, marked) {
                this.marked = marked;
                this.$sce = $sce;
            }
            $onInit() {
                const {classItem: classItem, $sce: $sce, marked: marked} = this;
                if (!classItem.props) return;
                for (let index = 0; index < classItem.props.length; index++) {
                    const prop = classItem.props[index];
                    prop.description = $sce.trustAsHtml(marked(prop.description));
                }
            }
        } ],
        template: `\n<div class="params" ng-if="$ctrl.classItem.props !== undefined">\n  <ul>\n    <li ng-repeat="$prop in $ctrl.classItem.props">\n      <div>\n        <a href="#" ng-bind="$prop.type"></a> <code class="param-name" ng-bind="$prop.name"></code>\n      </div>\n      <div ng-if="$prop.description !== undefined" class="param-description">\n        <p class="description markdown-body" ng-bind-html="$prop.description"></p>\n      </div>\n    </li>\n  </ul>\n</div>\n`
    });
})();

(function() {
    "use strict";
    const module = angular.module("app.yuidoc.elements");
    module.component("classItem", {
        bindings: {
            module: "<",
            class: "<",
            classItem: "<"
        },
        controller: [ "$sce", "marked", class ClassItemComponent {
            constructor($sce, marked) {
                this.$sce = $sce;
                this.marked = marked;
            }
            $onInit() {
                const {$sce: $sce, classItem: classItem, marked: marked} = this;
                if (classItem.description) {
                    classItem.description = $sce.trustAsHtml(marked(classItem.description));
                }
                if (classItem.example) {
                    for (let index = 0; index < classItem.example.length; index++) {
                        classItem.example[index] = $sce.trustAsHtml(marked(classItem.example[index]));
                    }
                }
            }
            getTypeClasses() {
                const {classItem: classItem} = this;
                const classes = [ "type", "type--text-top", `type__${classItem.itemtype}--text` ];
                if (classItem.static === 1) classes.push(`type--static-text`);
                return classes.join(" ");
            }
        } ],
        template: `\n<div class="panel">\n\n  <div class="panel__head">\n    <h3 id="{{$ctrl.module.name | encodeId}}_{{$ctrl.class.name | encodeId}}_{{$ctrl.classItem.name | encodeId}}" class="my-3">\n      <span ng-bind="$ctrl.classItem.name"></span>\n      <i class="{{$ctrl.getTypeClasses()}}"></i>\n    </h3>\n    \x3c!--\n    <div>\n      <span ng-if="$ctrl.classItem.access!==undefined" class="badge badge--dark" ng-bind="$ctrl.classItem.access"></span>\n      <span ng-if="$ctrl.classItem.access===undefined" class="badge badge--dark">public</span>\n      <span ng-if="$ctrl.classItem.async===1" class="badge badge--dark">async</span>\n    </div>\n    --!>\n\n    <br />\n    <div ng-if="$ctrl.classItem.description !== undefined" \n      class="summary description markdown-body" \n      ng-bind-html="$ctrl.classItem.description"></div>\n  </div>\n\n  <div class="panel__body">\n\n    <ng-switch on="$ctrl.classItem.itemtype">\n\n      <span ng-switch-when="method">\n        <class-item-method class-item="$ctrl.classItem"></class-item-method>\n      </span>\n\n      <span ng-switch-when="params">\n        <class-item-params class-item="$ctrl.classItem"></class-item-params>\n      </span>\n\n      <span ng-switch-when="property">\n        <class-item-property class-item="$ctrl.classItem"></class-item-property>\n      </span>\n\n      <span ng-switch-when="attribute">\n        <class-item-attribute class-item="$ctrl.classItem"></class-item-attribute>\n      </span>\n\n    </ng-switch>\n\n    <div class="example" ng-if="$ctrl.classItem.example !== undefined">\n      <strong class="text-muted">Examples</strong>\n      <br />\n      <br />\n\n      <div ng-repeat="exampleItem in $ctrl.classItem.example">\n        <div class="markdown-body" ng-bind-html="exampleItem"></div>\n      </div>\n    </div>\n    \n    <br />\n    <br />\n</div>\n`
    });
})();

(function() {
    "use strict";
    const module = angular.module("app.yuidoc.elements");
    module.component("class", {
        bindings: {
            module: "<",
            class: "<"
        },
        controller: [ "$sce", "marked", "yuidocDataService", class ClassComponent {
            constructor($sce, marked, yuidocDataService) {
                this.$sce = $sce;
                this.marked = marked;
                this.yuidocDataService = yuidocDataService;
            }
            $onInit() {
                const {["class"]: $class, $sce: $sce, marked: marked} = this;
                if ($class.description) {
                    $class.description = $sce.trustAsHtml(marked($class.description));
                }
            }
            getClassItemsByModuleAndClassName(moduleName, className) {
                return this.yuidocDataService.getClassItemsByModuleAndClassName(moduleName, className);
            }
        } ],
        template: `\n\n<div class="panel">\n\n  <div class="panel__head">\n    <h2 id="{{$ctrl.module.name | encodeId}}_{{$ctrl.class.name | encodeId}}" class="my-3">\n      <span ng-bind="$ctrl.class.name"></span>\n      <i class="type type__class type__class--text"></i>\n    </h2>\n    <div ng-if="$ctrl.class.extends.length > 0">\n      Extends <a href="#" ng-bind="$ctrl.class.extends"></a>\n    </div>\n    \x3c!--\n    <div>\n      <span ng-if="$ctrl.class.access!==undefined" class="badge badge--dark" ng-bind="$ctrl.class.access"></span>\n      <span ng-if="$ctrl.class.access===undefined" class="badge badge--dark">public</span>\n    </div>\n    --!>\n    <div ng-if="$ctrl.class.description !== undefined" class="summary description markdown-body"\n        ng-bind-html="$ctrl.class.description"></div>\n  </div>\n\n  <div class="panel__body panel--pad-left">\n\n    <div ng-if="$ctrl.class.is_constructor===1">\n      <strong class="text-muted">Constructor Syntax</strong>\n      <div class="bs-callout markdown-body">\n        <code>new <span ng-bind="$ctrl.class.name"></span>(<span ng-repeat="$param in $ctrl.class.params">\n            <span ng-bind="$param.name"></span><span ng-if="$index < $ctrl.class.params.length-1">, </span>\n        </span>);</code>\n      </div>\n    </div>\n\n    <div class="classItem">\n      <div ng-repeat="$classItem in $ctrl.getClassItemsByModuleAndClassName($ctrl.module.name, $ctrl.class.name) | orderBy:['itemtype', 'static', 'name']">\n        <class-item module="$ctrl.module" class="$ctrl.class" class-item="$classItem"></class-item>\n      </div>\n    </div>\n\n  </div>\n\n</div>\n`
    });
})();

(function() {
    "use strict";
    const controllers = angular.module("app.yuidoc.elements");
    controllers.component("yuiDocument", {
        controller: [ "$timeout", "yuidocDataService", class YUIDocumentComponent {
            constructor($timeout, yuidocDataService) {
                this.$timeout = $timeout;
                this.yuidocDataService = yuidocDataService;
                this.getClassesByModuleName = yuidocDataService.getClassesByModuleName.bind(yuidocDataService);
                this.getClassItemsByModuleAndClassName = yuidocDataService.getClassItemsByModuleAndClassName.bind(yuidocDataService);
            }
            $onInit() {
                const {yuidocDataService: yuidocDataService} = this;
                this.resolved = false;
                this.modules = {};
                this.classes = {};
                this.project = {};
                this.classItems = [];
                this.globalClasses = [];
                yuidocDataService.load().then(() => {
                    this.modules = yuidocDataService.getModules();
                    this.project = yuidocDataService.getProject();
                    this.classes = yuidocDataService.getClasses();
                    this.classItems = yuidocDataService.getClassItems();
                    this.globalClasses = yuidocDataService.getGlobalClasses();
                    this.resolved = true;
                });
            }
        } ],
        template: `\n<header class="navbar navbar-dark bg-dark navbar-expand-lg bd-navbar">\n  <app-header project="$ctrl.project"></app-header>\n</header>\n\n<div class="container-fluid" ng-class="{hidden: !$ctrl.resolved}">\n\n  <div class="row flex-xl-nowrap">\n\n    <div id="apiSidebar" class="col-12 col-md-3 col-xl-2 bd-sidebar" role="navigation">\n      <filter-sidebar modules="$ctrl.modules" global-classes="$ctrl.globalClasses"></filter-sidebar>\n    </div>\n\n    <main class="col-12 col-md-9 col-xl-8 py-md-3 pl-md-5 bd-content">\n      <module-list modules="$ctrl.modules"></module-list>\n      \x3c!--<global-classes global-classes="$ctrlglobalClasses"></global-classes>--\x3e\n    </main>\n\n  </div>\n</div>\n`
    });
})();

(function() {
    "use strict";
    const module = angular.module("app.yuidoc.elements");
    module.component("classItemsList", {
        bindings: {
            module: "<",
            className: "<",
            filterText: "<"
        },
        controller: [ "yuidocDataService", class ClassItemsListComponent {
            constructor(yuidocDataService) {
                this.yuidocDataService = yuidocDataService;
            }
            getClassItemsByModuleAndClassName(moduleName, className) {
                return this.yuidocDataService.getClassItemsByModuleAndClassName(moduleName, className);
            }
            getTypeClasses(classItem) {
                const classes = [ "type", `type__${classItem.itemtype}--text-initial` ];
                if (classItem.static === 1) classes.push(`type--static-colour`);
                return classes.join(" ");
            }
        } ],
        template: `\n<tree-node\n    ng-repeat="$classItem in $ctrl.getClassItemsByModuleAndClassName($ctrl.module.name, $ctrl.className) | filter:{'name':$ctrl.filterText} | orderBy:['itemtype', 'static', 'name']">\n\n  <tree-item skin="default"\n    ng-href="#{{$ctrl.module.name | encodeId}}_{{$ctrl.className | encodeId}}_{{$classItem.name | encodeId}}"\n    scroll-to>\n    <i class="{{$ctrl.getTypeClasses($classItem)}}"></i><span ng-bind="$classItem.name"></span>\n  </tree-item>\n\n</tree-node>\n`
    });
})();

(function() {
    "use strict";
    const module = angular.module("app.yuidoc.elements");
    module.component("filterSidebar", {
        bindings: {
            modules: "<",
            globalClasses: "<"
        },
        controller: [ "yuidocDataService", class FilterSidebarComponent {
            constructor(yuidocDataService) {
                this.yuidocDataService = yuidocDataService;
            }
            $onInit() {
                this.filterText = "";
            }
            getClassesByModuleName(moduleName) {
                const results = this.yuidocDataService.getClassesByModuleName(moduleName);
                return results;
            }
            getClassItemsByModuleAndClassName(moduleName, className) {
                return this.yuidocDataService.getClassItemsByModuleAndClassName(moduleName, className);
            }
            getGlobalClasses() {
                const sortedByKeysMap = {};
                Object.keys(this.globalClasses).sort().forEach(key => sortedByKeysMap[key] = this.globalClasses[key]);
                return sortedByKeysMap;
            }
            filterChanged(text) {
                this.filterText = text;
            }
        } ],
        template: `\n<sidebar data-on-filter-changed="$ctrl.filterChanged(text)">\n\n  <tree>\n\n    <tree-list ng-if="$ctrl.modules!==undefined">\n\n      <tree-node ng-repeat="$module in $ctrl.modules | orderByKeys:['name']">\n        <tree-item skin="default"\n                   ng-href="#{{$module.name | encodeId}}"\n                   scroll-to>\n          <i class="type type__module--text-initial"></i><span ng-bind="$module.name"></span>\n        </tree-item>\n\n        <tree-list id="{{$module.name}}_properties">\n          <tree-node ng-repeat="($className, class) in $ctrl.getClassesByModuleName($module.name) | orderByKeys:['name']">\n            <tree-item skin="default"\n                       ng-href="#{{$module.name | encodeId}}_{{$className | encodeId}}"\n                       scroll-to>\n              <i class="type type__class--text-initial"></i><span ng-bind="$className"></span>\n            </tree-item>\n            <tree-list id="{{$module.name}}_{{$className}}_properties">\n              <class-items-list module="$module" class-name="$className" filter-text="$ctrl.filterText"></class-items-list>\n            </tree-list>\n          </tree-node>\n        </tree-list>\n\n      </tree-node>\n    </tree-list>\n\n    <tree-list ng-if="$ctrl.globalClasses!==undefined">\n      <tree-node ng-repeat="($className, class) in $ctrl.getGlobalClasses()">\n        <tree-item skin="default" ng-href="#{{$className | encodeId}}" scroll-to>\n          <i class="type type__class--text-initial"></i><span ng-bind="$className"></span>\n        </tree-item>\n        <tree-list id="{{$className}}_properties">\n          <class-items-list module="{}" class-name="$className"></class-items-list>\n        </tree-list>\n      </tree-node>\n    </tree-list>\n\n  </tree>\n</sidebar>\n`
    });
})();

(function() {
    "use strict";
    const module = angular.module("app.yuidoc.elements");
    module.component("globalClasses", {
        templateUrl: "assets/templates/global-classes-template.html",
        bindings: {
            globalClasses: "<"
        },
        controller: class GlobalClassesComponent {},
        template: `\n<div class="classes" ng-repeat="(key, $class) in $ctrl.globalClasses | orderBy:['name']">\n  <class module="{name: undefined}" class="$class"></class>\n</div>\n`
    });
})();

(function() {
    "use strict";
    const module = angular.module("app.yuidoc.elements");
    module.component("moduleList", {
        bindings: {
            modules: "="
        },
        controller: class ModuleListComponent {},
        template: `\n<div class="modules" ng-repeat="$module in $ctrl.modules">\n  <module module="$module"></module>\n</div>\n`
    });
})();

(function() {
    "use strict";
    const module = angular.module("app.yuidoc.elements");
    module.component("module", {
        bindings: {
            module: "<"
        },
        controller: [ "$sce", "marked", "yuidocDataService", class ModuleComponent {
            constructor($sce, marked, yuidocDataService) {
                this.$sce = $sce;
                this.marked = marked;
                this.yuidocDataService = yuidocDataService;
            }
            $onInit() {
                const {module: module, $sce: $sce, marked: marked} = this;
                if (module.description) {
                    module.description = $sce.trustAsHtml(marked(module.description));
                }
            }
            getClassesByModuleName(moduleName) {
                return this.yuidocDataService.getClassesByModuleName(moduleName);
            }
        } ],
        template: `\n<div id="{{$ctrl.module.name | encodeId}}">\n  <h1 class="my-4">\n    <span ng-bind="$ctrl.module.name"></span>\n  </h1>\n</div>\n\n<div ng-if="$ctrl.module.description !== undefined"\n     class="summary description markdown-body"\n     ng-bind-html="$ctrl.module.description">\n</div>\n\n<div class="classes" ng-repeat="($key, $class) in $ctrl.getClassesByModuleName($ctrl.module.name) | orderByKeys">\n  <class module="$ctrl.module" class="$class"></class>\n</div>\n`
    });
})();

(function() {
    "use strict";
    const app = angular.module("app", [ "app.core", "app.yuidoc" ]);
    marked.setOptions({
        highlight: function(code) {
            return hljs.highlightAuto(code).value;
        }
    });
    app.constant("marked", window.marked);
    app.constant("hljs", window.hljs);
    app.constant("yuidocDataPath", "data.json");
    app.constant("querifySync", querify.sync);
})();
//# sourceMappingURL=app-bundle.js.map