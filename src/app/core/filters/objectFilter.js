(function () {
  'use strict'

  const filters = angular.module('app.core.filters')

  filters.filter('objectFilter', function () {
    return function objectFilter(items, filter) {
      debugger
      if (!filter)
        return items

      const result = {}
      angular.forEach(filter, function (filterVal, filterKey) {

        angular.forEach(items, function (item, key) {
          const fieldVal = item[filterKey]
          if (fieldVal && fieldVal.toLowerCase().indexOf(filterVal.toLowerCase()) > -1)
            result[key] = item
        })

      })

      return result
    }

  })

}())