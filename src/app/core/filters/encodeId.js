(function () {
  'use strict'

  const filters = angular.module('app.core.filters')

  filters.filter('encodeId', function () {
    return function encodeId(inValue) {
      if (inValue === undefined || inValue.length === 0)
        return inValue
      else
        return inValue.replace(/[$]/g, 'dollar_')
    }
  })

}())