(function () {
  'use strict'

  const filters = angular.module('app.core.filters')

  filters.filter('orderByKeys', function () {

    return function orderByKeys(objectToSort) {
      const sortedByKeysMap = {}
      Object.keys(objectToSort)
        .sort()
        .forEach(key => sortedByKeysMap[key] = objectToSort[key])

      return sortedByKeysMap
    }

  })

})()