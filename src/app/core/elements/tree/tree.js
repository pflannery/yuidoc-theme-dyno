(function () {
  'use strict'

  const module = angular.module('app.core.elements')

  module.component('tree', {
    transclude: true,
    controller: class TreeComponent { },
    template: '<div class="tree" ng-transclude></div>'
  })

  module.component('treeList', {
    transclude: true,
    controller: class TreeListComponent { },
    template: '<ul class="tl" role="menu" ng-transclude></ul>'
  })

  module.component('treeNode', {
    transclude: true,
    controller: class TreeNodeComponent { },
    template: '<li class="tn" role="listitem" ng-transclude></li>'
  })

  module.component('treeItem', {
    transclude: true,
    controller: class TreeItemComponent { },
    template: '<a class="ti ti-{{skin}}" role="link" ng-transclude></a>',
    scope: {
      skin: '@'
    }
  })

})()