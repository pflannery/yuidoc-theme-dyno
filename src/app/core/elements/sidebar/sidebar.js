(function () {
  'use strict'

  const module = angular.module('app.core.elements')

  module.component('sidebar', {
    transclude: true,
    bindings: {
      onFilterChanged: '&'
    },
    controller: class TreeSidebarComponent {
      $onInit() {
        this.filterText = ''
      }
    },
    template: `
<form class="bd-search d-flex align-items-center">
  <input class="form-control" type="text" ng-model="$ctrl.filterText" ng-change="$ctrl.onFilterChanged({text: $ctrl.filterText})" placeholder="Filter items">
</form>
<nav class="collapse bd-links" ng-transclude></div>
`
  })

})()
