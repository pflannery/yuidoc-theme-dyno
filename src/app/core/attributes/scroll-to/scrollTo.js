(function () {
  'use strict';

  const module = angular.module('app.core.attributes')

  module.directive('scrollTo', function () {
    return {
      restrict: 'A',
      link: function (scope, element, attrs) {

        element.bind('click', function (event) {
          let scrollToSelector = null;
          let scrollToOffset = 0;
          let preventDefault = false;

          if (attrs.href !== undefined && attrs.href !== '') {
            preventDefault = true;
            scrollToSelector = attrs.href;
          } else {
            scrollToSelector = attrs.scrollTo;
          }

          attrs.scrollToOffset = attrs.scrollToOffset || $('body').attr('scroll-to-offset');
          if (attrs.scrollToOffset !== undefined &&
            attrs.scrollToOffset !== '') {
            scrollToOffset = attrs.scrollToOffset;
          }

          // check a selector is present
          if (scrollToSelector === undefined ||
            scrollToSelector === '') {
            return;
          }

          const $scrollToSelector = $(scrollToSelector);

          // check the selector exists
          if ($scrollToSelector.length === 0) {
            throw new Error('scrollTo directive: ' + scrollToSelector + ' does not exist.');
          }

          // get all before\after elements inside the scroll scope
          const beforeScrollTo = $('[before-scroll-to]', $scrollToSelector);
          const afterScrollTo = $('[after-scroll-to]', $scrollToSelector);

          // get the top offset to scroll too
          const scrollToTop = $scrollToSelector.offset().top;

          // get the top most element's top offset
          const currentTop = $('header').offset().top;

          // based on the current top offset set the duration of scroll animation
          const duration = (currentTop !== (scrollToTop - scrollToOffset)) ? 1000 : 0;

          $('html, body').stop().animate({
            scrollTop: scrollToTop - scrollToOffset
          }, {
              duration,
              easing: 'easeInOutExpo',
              start: function () {
                let index;
                let $element;

                for (index = 0; index < afterScrollTo.length; index++) {
                  $element = $(afterScrollTo[index]);
                  $element.removeClass($element.attr('after-scroll-to'));
                }

                for (index = 0; index < beforeScrollTo.length; index++) {
                  $element = $(beforeScrollTo[index]);
                  $element.addClass($element.attr('before-scroll-to'));
                }

              },
              complete: function () {
                let index;
                let $element;

                for (index = 0; index < beforeScrollTo.length; index++) {
                  $element = $(beforeScrollTo[index]);
                  $element.removeClass($element.attr('before-scroll-to'));
                }

                for (index = 0; index < afterScrollTo.length; index++) {
                  $element = $(afterScrollTo[index]);
                  $element.addClass($element.attr('after-scroll-to'));
                }
              }
            }
          );

          if (preventDefault === true) {
            event.preventDefault();
          }
        });
      }
    };
  });
})();
