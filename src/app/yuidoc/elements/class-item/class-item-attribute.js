(function () {
  'use strict'

  const module = angular.module('app.yuidoc.elements');

  module.component('classItemAttribute', {
    binding: {
      classItem: '<'
    },
    controller: class classItemAttributeComponent { },
    template: `
    <div>
      <div class="bs-callout markdown-body">
        <a href="#" ng-bind="$ctrl.classItem.type"></a>
        <span ng-bind="$ctrl.classItem.name"></span>
        <span ng-if="$ctrl.classItem.access==='private'" class="badge badge--dark">private</span>
      </div>
    </div>
`
  })

})()