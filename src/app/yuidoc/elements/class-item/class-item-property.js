(function () {
  'use strict'

  const module = angular.module('app.yuidoc.elements');

  module.component('classItemProperty', {
    bindings: {
      classItem: '<'
    },
    controller: class ClassItemPropertyComponent { },
    template: `
<div>
  <ul>
    <li>
      <a href="#" ng-bind="$ctrl.classItem.type"></a> <span ng-bind="$ctrl.classItem.name"></span>
      <class-item-params ng-if="$ctrl.classItem.params !== undefined" class-item="$ctrl.classItem"></class-item-params>
    </li>
  </ul>
</div>
`
  })

})()
