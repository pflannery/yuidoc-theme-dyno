(function () {
  'use strict'

  const module = angular.module('app.yuidoc.elements');

  module.component('classItemParams', {

    bindings: {
      classItem: '<'
    },
    controller: ["$sce", "marked", class ClassItemParamsComponent {

      constructor($sce, marked) {
        this.marked = marked
        this.$sce = $sce
      }

      $onInit() {
        const { $sce, classItem, marked } = this;
        // make each param description safe to parse as html
        if (!classItem.params)
          return

        for (let index = 0; index < classItem.params.length; index++) {
          const param = classItem.params[index];
          param.description = $sce.trustAsHtml(
            marked(param.description)
          )
        }

      }

    }],
    template: `
<div class="params" ng-if="$ctrl.classItem.params !== undefined">
  <div ng-if="$ctrl.classItem.itemtype==='method'">
    <strong class="text-muted">Parameters</strong>
    <br/>
  </div>
  <br/>
  <ul>
    <li ng-repeat="$param in $ctrl.classItem.params">
      <div>
        <a href="#" ng-bind="$param.type"></a> <code class="param-name" ng-bind="$param.name"></code>
      </div>
      <div ng-if="$param.description !== undefined" class="param-description">
        <p class="description markdown-body" ng-bind-html="$param.description"></p>
      </div>
      <class-item-props ng-if="$param.props !== undefined" class-item="$param"></class-item-props>
    </li>
  </ul>
</div>
`
  })

})()
