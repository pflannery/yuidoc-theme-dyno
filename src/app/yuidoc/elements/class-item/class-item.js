(function () {
  'use strict'

  const module = angular.module('app.yuidoc.elements');

  module.component('classItem', {

    bindings: {
      module: '<',
      class: '<',
      classItem: '<'
    },
    controller: ["$sce", "marked", class ClassItemComponent {

      constructor($sce, marked) {
        this.$sce = $sce
        this.marked = marked
      }

      $onInit() {
        const { $sce, classItem, marked } = this

        // make the description safe to parse as html
        if (classItem.description) {
          classItem.description = $sce.trustAsHtml(
            marked(classItem.description)
          )
        }

        // make all example item descriptions safe to parse as html
        if (classItem.example) {
          for (let index = 0; index < classItem.example.length; index++) {
            classItem.example[index] = $sce.trustAsHtml(
              marked(classItem.example[index])
            )
          }
        }
      }

      getTypeClasses() {
        const { classItem } = this
        const classes = [
          "type",
          "type--text-top",
          `type__${classItem.itemtype}--text`
        ]

        if (classItem.static === 1) classes.push(`type--static-text`)

        return classes.join(' ')
      }

    }],
    template: `
<div class="panel">

  <div class="panel__head">
    <h3 id="{{$ctrl.module.name | encodeId}}_{{$ctrl.class.name | encodeId}}_{{$ctrl.classItem.name | encodeId}}" class="my-3">
      <span ng-bind="$ctrl.classItem.name"></span>
      <i class="{{$ctrl.getTypeClasses()}}"></i>
    </h3>
    <!--
    <div>
      <span ng-if="$ctrl.classItem.access!==undefined" class="badge badge--dark" ng-bind="$ctrl.classItem.access"></span>
      <span ng-if="$ctrl.classItem.access===undefined" class="badge badge--dark">public</span>
      <span ng-if="$ctrl.classItem.async===1" class="badge badge--dark">async</span>
    </div>
    --!>

    <br />
    <div ng-if="$ctrl.classItem.description !== undefined" 
      class="summary description markdown-body" 
      ng-bind-html="$ctrl.classItem.description"></div>
  </div>

  <div class="panel__body">

    <ng-switch on="$ctrl.classItem.itemtype">

      <span ng-switch-when="method">
        <class-item-method class-item="$ctrl.classItem"></class-item-method>
      </span>

      <span ng-switch-when="params">
        <class-item-params class-item="$ctrl.classItem"></class-item-params>
      </span>

      <span ng-switch-when="property">
        <class-item-property class-item="$ctrl.classItem"></class-item-property>
      </span>

      <span ng-switch-when="attribute">
        <class-item-attribute class-item="$ctrl.classItem"></class-item-attribute>
      </span>

    </ng-switch>

    <div class="example" ng-if="$ctrl.classItem.example !== undefined">
      <strong class="text-muted">Examples</strong>
      <br />
      <br />

      <div ng-repeat="exampleItem in $ctrl.classItem.example">
        <div class="markdown-body" ng-bind-html="exampleItem"></div>
      </div>
    </div>
    
    <br />
    <br />
</div>
`
  })

})()
