(function () {
  'use strict'

  const module = angular.module('app.yuidoc.elements')

  module.component('classItemProps', {
    bindings: {
      classItem: '<'
    },
    controller: ["$sce", "marked", class ClassItemPropsComponent {

      constructor($sce, marked) {
        this.marked = marked
        this.$sce = $sce
      }

      $onInit() {
        const { classItem, $sce, marked } = this
        if (!classItem.props)
          return

        // make each param description safe to parse as html
        for (let index = 0; index < classItem.props.length; index++) {
          const prop = classItem.props[index];
          prop.description = $sce.trustAsHtml(
            marked(prop.description)
          )
        }
      }

    }],
    template: `
<div class="params" ng-if="$ctrl.classItem.props !== undefined">
  <ul>
    <li ng-repeat="$prop in $ctrl.classItem.props">
      <div>
        <a href="#" ng-bind="$prop.type"></a> <code class="param-name" ng-bind="$prop.name"></code>
      </div>
      <div ng-if="$prop.description !== undefined" class="param-description">
        <p class="description markdown-body" ng-bind-html="$prop.description"></p>
      </div>
    </li>
  </ul>
</div>
`
  })

})()