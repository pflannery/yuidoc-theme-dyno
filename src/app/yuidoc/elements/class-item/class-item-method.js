(function () {
  'use strict'

  const module = angular.module('app.yuidoc.elements');

  module.component('classItemMethod', {
    bindings: {
      classItem: '<'
    },
    controller: ["$sce", "marked", class ClassItemMethodComponent {

      constructor($sce, marked) {
        this.$sce = $sce
        this.marked = marked
      }

      $onInit() {
        const { classItem, marked, $sce } = this
        // make the return description safe to parse as html
        if (classItem.return && classItem.return.description) {
          classItem.return.description = $sce.trustAsHtml(
            marked(classItem.return.description)
          )
        }
      }

    }],
    template: `
<div>
  <div class="syntax">
    <strong class="text-muted">Syntax</strong>

    <div class="bs-callout markdown-body">
      <code>
        <span ng-bind="$ctrl.classItem.name"></span>(<span ng-repeat="$param in $ctrl.classItem.params">
            <span ng-bind="$param.name"></span><span ng-if="$index < $ctrl.classItem.params.length-1">, </span>
        </span>);
      </code>
    </div>
  </div>

  <class-item-params class-item="$ctrl.classItem"></class-item-params>

  <div class="returns" ng-if="$ctrl.classItem.return !== undefined">
    <strong class="text-muted">Returns</strong>
    <br/><br/>
    <ul>
      <li>
        <a href="#" ng-bind="$ctrl.classItem.return.type"></a>

        <div class="param-description">
          <p class="description markdown-body" ng-bind-html="$ctrl.classItem.return.description"></p>
        </div>
      </li>
    </ul>
  </div>

</div>
`
  })

})()