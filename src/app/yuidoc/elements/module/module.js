(function () {
  'use strict'

  const module = angular.module('app.yuidoc.elements')

  module.component('module', {
    bindings: {
      module: '<'
    },
    controller: ["$sce", "marked", "yuidocDataService", class ModuleComponent {

      constructor($sce, marked, yuidocDataService) {
        this.$sce = $sce
        this.marked = marked
        this.yuidocDataService = yuidocDataService
      }

      $onInit() {
        const { module, $sce, marked } = this
        if (module.description) {
          module.description = $sce.trustAsHtml(
            marked(module.description)
          )
        }
      }

      getClassesByModuleName(moduleName) {
        return this.yuidocDataService.getClassesByModuleName(moduleName)
      }

    }],
    template: `
<div id="{{$ctrl.module.name | encodeId}}">
  <h1 class="my-4">
    <span ng-bind="$ctrl.module.name"></span>
  </h1>
</div>

<div ng-if="$ctrl.module.description !== undefined"
     class="summary description markdown-body"
     ng-bind-html="$ctrl.module.description">
</div>

<div class="classes" ng-repeat="($key, $class) in $ctrl.getClassesByModuleName($ctrl.module.name) | orderByKeys">
  <class module="$ctrl.module" class="$class"></class>
</div>
`
  })

})()
