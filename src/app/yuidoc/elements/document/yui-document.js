(function () {
  'use strict'

  const controllers = angular.module('app.yuidoc.elements');

  controllers.component('yuiDocument', {
    controller: ["$timeout", "yuidocDataService", class YUIDocumentComponent {

      constructor($timeout, yuidocDataService) {
        this.$timeout = $timeout
        this.yuidocDataService = yuidocDataService

        this.getClassesByModuleName = yuidocDataService.getClassesByModuleName.bind(yuidocDataService)
        this.getClassItemsByModuleAndClassName = yuidocDataService.getClassItemsByModuleAndClassName.bind(yuidocDataService)
      }

      $onInit() {
        const { yuidocDataService } = this

        this.resolved = false
        this.modules = {}
        this.classes = {}
        this.project = {}

        this.classItems = []
        this.globalClasses = []

        yuidocDataService.load()
          .then(() => {
            this.modules = yuidocDataService.getModules()
            this.project = yuidocDataService.getProject()
            this.classes = yuidocDataService.getClasses()
            this.classItems = yuidocDataService.getClassItems()
            this.globalClasses = yuidocDataService.getGlobalClasses()
            this.resolved = true
          })
      }

    }],
    template: `
<header class="navbar navbar-dark bg-dark navbar-expand-lg bd-navbar">
  <app-header project="$ctrl.project"></app-header>
</header>

<div class="container-fluid" ng-class="{hidden: !$ctrl.resolved}">

  <div class="row flex-xl-nowrap">

    <div id="apiSidebar" class="col-12 col-md-3 col-xl-2 bd-sidebar" role="navigation">
      <filter-sidebar modules="$ctrl.modules" global-classes="$ctrl.globalClasses"></filter-sidebar>
    </div>

    <main class="col-12 col-md-9 col-xl-8 py-md-3 pl-md-5 bd-content">
      <module-list modules="$ctrl.modules"></module-list>
      <!--<global-classes global-classes="$ctrlglobalClasses"></global-classes>-->
    </main>

  </div>
</div>
`
  })

})()
