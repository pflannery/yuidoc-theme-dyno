(function () {
  'use strict'

  const module = angular.module('app.yuidoc.elements')

  module.component('filterSidebar', {
    bindings: {
      modules: '<',
      globalClasses: '<'
    },
    controller: ["yuidocDataService", class FilterSidebarComponent {

      constructor(yuidocDataService) {
        this.yuidocDataService = yuidocDataService
      }

      $onInit() {
        this.filterText = ''
      }

      getClassesByModuleName(moduleName) {
        const results = this.yuidocDataService.getClassesByModuleName(moduleName)

        return results
      }

      getClassItemsByModuleAndClassName(moduleName, className) {
        return this.yuidocDataService.getClassItemsByModuleAndClassName(moduleName, className)
      }

      getGlobalClasses() {
        const sortedByKeysMap = {}
        Object.keys(this.globalClasses)
          .sort()
          .forEach(key => sortedByKeysMap[key] = this.globalClasses[key])

        return sortedByKeysMap
      }

      filterChanged(text) {
        this.filterText = text
      }

    }],
    template: `
<sidebar data-on-filter-changed="$ctrl.filterChanged(text)">

  <tree>

    <tree-list ng-if="$ctrl.modules!==undefined">

      <tree-node ng-repeat="$module in $ctrl.modules | orderByKeys:['name']">
        <tree-item skin="default"
                   ng-href="#{{$module.name | encodeId}}"
                   scroll-to>
          <i class="type type__module--text-initial"></i><span ng-bind="$module.name"></span>
        </tree-item>

        <tree-list id="{{$module.name}}_properties">
          <tree-node ng-repeat="($className, class) in $ctrl.getClassesByModuleName($module.name) | orderByKeys:['name']">
            <tree-item skin="default"
                       ng-href="#{{$module.name | encodeId}}_{{$className | encodeId}}"
                       scroll-to>
              <i class="type type__class--text-initial"></i><span ng-bind="$className"></span>
            </tree-item>
            <tree-list id="{{$module.name}}_{{$className}}_properties">
              <class-items-list module="$module" class-name="$className" filter-text="$ctrl.filterText"></class-items-list>
            </tree-list>
          </tree-node>
        </tree-list>

      </tree-node>
    </tree-list>

    <tree-list ng-if="$ctrl.globalClasses!==undefined">
      <tree-node ng-repeat="($className, class) in $ctrl.getGlobalClasses()">
        <tree-item skin="default" ng-href="#{{$className | encodeId}}" scroll-to>
          <i class="type type__class--text-initial"></i><span ng-bind="$className"></span>
        </tree-item>
        <tree-list id="{{$className}}_properties">
          <class-items-list module="{}" class-name="$className"></class-items-list>
        </tree-list>
      </tree-node>
    </tree-list>

  </tree>
</sidebar>
`
  })

})()