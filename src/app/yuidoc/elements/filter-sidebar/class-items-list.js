(function () {
  'use strict'

  const module = angular.module('app.yuidoc.elements')

  module.component('classItemsList', {
    bindings: {
      module: '<',
      className: '<',
      filterText: '<'
    },
    controller: ["yuidocDataService", class ClassItemsListComponent {

      constructor(yuidocDataService) {
        this.yuidocDataService = yuidocDataService
      }

      getClassItemsByModuleAndClassName(moduleName, className) {
        return this.yuidocDataService.getClassItemsByModuleAndClassName(moduleName, className)
      }

      getTypeClasses(classItem) {
        const classes = [
          "type",
          `type__${classItem.itemtype}--text-initial`
        ]

        if (classItem.static === 1) classes.push(`type--static-colour`)

        return classes.join(' ')
      }

    }],
    template: `
<tree-node
    ng-repeat="$classItem in $ctrl.getClassItemsByModuleAndClassName($ctrl.module.name, $ctrl.className) | filter:{'name':$ctrl.filterText} | orderBy:['itemtype', 'static', 'name']">

  <tree-item skin="default"
    ng-href="#{{$ctrl.module.name | encodeId}}_{{$ctrl.className | encodeId}}_{{$classItem.name | encodeId}}"
    scroll-to>
    <i class="{{$ctrl.getTypeClasses($classItem)}}"></i><span ng-bind="$classItem.name"></span>
  </tree-item>

</tree-node>
`
  })

})()