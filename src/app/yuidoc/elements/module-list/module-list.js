(function () {
  'use strict';

  const module = angular.module('app.yuidoc.elements')

  module.component('moduleList', {
    bindings: {
      modules: '='
    },
    controller: class ModuleListComponent { },
    template: `
<div class="modules" ng-repeat="$module in $ctrl.modules">
  <module module="$module"></module>
</div>
`
  })

})()
