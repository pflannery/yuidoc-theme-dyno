(function () {
  'use strict'

  const module = angular.module('app.yuidoc.elements')

  module.component('class', {

    bindings: {
      module: '<',
      class: '<'
    },
    controller: ["$sce", "marked", "yuidocDataService", class ClassComponent {

      constructor($sce, marked, yuidocDataService) {
        this.$sce = $sce
        this.marked = marked
        this.yuidocDataService = yuidocDataService
      }

      $onInit() {
        const { ['class']: $class, $sce, marked } = this
        if ($class.description) {
          $class.description = $sce.trustAsHtml(
            marked($class.description)
          )
        }
      }

      getClassItemsByModuleAndClassName(moduleName, className) {
        return this.yuidocDataService.getClassItemsByModuleAndClassName(moduleName, className);
      }

    }],
    template: `

<div class="panel">

  <div class="panel__head">
    <h2 id="{{$ctrl.module.name | encodeId}}_{{$ctrl.class.name | encodeId}}" class="my-3">
      <span ng-bind="$ctrl.class.name"></span>
      <i class="type type__class type__class--text"></i>
    </h2>
    <div ng-if="$ctrl.class.extends.length > 0">
      Extends <a href="#" ng-bind="$ctrl.class.extends"></a>
    </div>
    <!--
    <div>
      <span ng-if="$ctrl.class.access!==undefined" class="badge badge--dark" ng-bind="$ctrl.class.access"></span>
      <span ng-if="$ctrl.class.access===undefined" class="badge badge--dark">public</span>
    </div>
    --!>
    <div ng-if="$ctrl.class.description !== undefined" class="summary description markdown-body"
        ng-bind-html="$ctrl.class.description"></div>
  </div>

  <div class="panel__body panel--pad-left">

    <div ng-if="$ctrl.class.is_constructor===1">
      <strong class="text-muted">Constructor Syntax</strong>
      <div class="bs-callout markdown-body">
        <code>new <span ng-bind="$ctrl.class.name"></span>(<span ng-repeat="$param in $ctrl.class.params">
            <span ng-bind="$param.name"></span><span ng-if="$index < $ctrl.class.params.length-1">, </span>
        </span>);</code>
      </div>
    </div>

    <div class="classItem">
      <div ng-repeat="$classItem in $ctrl.getClassItemsByModuleAndClassName($ctrl.module.name, $ctrl.class.name) | orderBy:['itemtype', 'static', 'name']">
        <class-item module="$ctrl.module" class="$ctrl.class" class-item="$classItem"></class-item>
      </div>
    </div>

  </div>

</div>
`
  })

})()
