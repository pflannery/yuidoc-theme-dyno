(function () {
  'use strict'

  const module = angular.module('app.yuidoc.elements')

  module.component('globalClasses', {
    templateUrl: 'assets/templates/global-classes-template.html',
    bindings: {
      globalClasses: '<'
    },
    controller: class GlobalClassesComponent { },
    template: `
<div class="classes" ng-repeat="(key, $class) in $ctrl.globalClasses | orderBy:['name']">
  <class module="{name: undefined}" class="$class"></class>
</div>
`
  })

})()
